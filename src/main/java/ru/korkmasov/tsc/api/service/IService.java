package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
