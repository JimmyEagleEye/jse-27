package ru.korkmasov.tsc.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @Nullable
    Date getCreated();

    void setCreated(@Nullable Date date);
}
