package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskByIdFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by id";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskById(user.getId(), id);
        if (task == null) throw new TaskNotFoundException();
    }

}

