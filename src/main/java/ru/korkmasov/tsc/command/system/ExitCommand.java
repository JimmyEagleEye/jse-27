package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ExitCommand extends AbstractCommand {

    @Nullable
    @Override
    public final String arg() {
        return null;
    }

    @NotNull
    @Override
    public final String name() {
        return "exit";
    }

    @NotNull
    @Override
    public final String description() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
